# cmps5P
# Project Maze Escape
# 1506456
from math import sqrt
from node import Node
from point import Point
from direction import Direction
from heapq import heappop, heappush

class AStar(object):
    def __init__(self,initialState, finalState, mazeMap):
        self.initialState = initialState
        self.finalState = finalState
        self.mazeMap = mazeMap

    def findSolution(self):
        queue = []
        path = []
        previous_position = {}
        initialPosition = (self.initialState.p[0],self.initialState.p[1])
        finalPosition = (self.finalState.p[0],self.finalState.p[1])
        distance = {initialPosition:0}
        heappush(queue,(distance[initialPosition],initialPosition))

        while queue:
            current_distance, current_position = heappop(queue)

            if current_position == finalPosition:
                while current_position != initialPosition:
                    path.append(current_position)
                    current_position = previous_position[current_position]
                path.append(initialPosition)
                return path

            currentNode = self.mazeMap.getNode(current_position[0],current_position[1])
            count = 0

            for hasAdjacent in currentNode.edges:
                if hasAdjacent:
                    if count == 0:
                        adjacent = (current_position[0],current_position[1] - 1)
                    elif count == 1:
                        adjacent = (current_position[0] + 1,current_position[1])
                    elif count == 2:
                        adjacent = (current_position[0],current_position[1] + 1)
                    elif count == 3:
                        adjacent = (current_position[0] - 1,current_position[1])

                    accumulated_distance = distance[current_position] + 1
                    if adjacent not in distance or accumulated_distance < distance[adjacent]:
                        distance[adjacent] = accumulated_distance
                        previous_position[adjacent] = current_position
                        heuristic_time = accumulated_distance + euclidean_distance(current_position[0],current_position[1],finalPosition[0],finalPosition[1])
                        heappush(queue,(heuristic_time,adjacent))

                count += 1

        return path

def euclidean_distance(p1x,p1y,p2x,p2y):
    distance = sqrt((p1x-p2x)**2+(p1y-p2y)**2)
    return distance