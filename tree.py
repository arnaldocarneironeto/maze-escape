# cmps5P
# Project Maze Escape
# 1506372

from numbers import Number
import random
from node import Node
from direction import Direction
from point import Point

class Tree(object):
    DEFAULT_SIZE = 100
    
    def __init__(self, width, height):
        if isinstance(width, Number) and isinstance(height, Number):
            self.width = int(width)
            self.height = int(height)
        else:
            self.width = DEFAULT_SIZE
            self.height = DEFAULT_SIZE
        self.nodeList = []
        for i in range(self.width):
            self.nodeList.append([])
            for j in range(self.height):
                self.nodeList[i].append(Node(i, j))
        self.solution = None

    def getWidth(self):
        return self.width

    def getHeight(self):
        return self.height

    def getNode(self, posX, posY):
        if isinstance(posX, Number) and isinstance(posY, Number):
            if int(posX) in range(self.width) and int(posY) in range(self.height):
                return self.nodeList[int(posX)][int(posY)]
            else:
                return None
        else:
            return None

    def getSolutionStepsCount(self):
        if self.solution != None:
            return self.solution.size()
        else:
            return 0

    def isThereStillVertex(self):
        result = False
        posX = 0
        while result == False and posX < self.width:
            posY = 0
            while result == False and posY < self.height:
                node = self.getNode(posX, posY)
                if node != None:
                    result = result or self.getNode(posX, posY).isOnGraph() == False
                posY += 1
            posX += 1
        return result

    def getSolution(self):
        return self.solution

    def generate(self, *args):
        if len(args) == 1:
            if isinstance(args[0], Number):
                random.seed(int(args[0]))
        self.setFirstRandomNode()
        while self.isThereStillVertex() == True:
            node = self.findNodeOutsideTree()
            self.putOnTree(node)

    def putOnTree(self, node):
        if isinstance(node, Node):
            dirsLeft = [n for n in range(4)]
            while node.isOnGraph() == False:
                n = random.randint(0, len(dirsLeft) - 1)
                dirc = dirsLeft[n]
                dirsLeft = dirsLeft[:n] + dirsLeft[n + 1:]
                if dirc == 0:
                    self.setEdge(node, Direction.UP, Direction.DOWN)
                elif dirc == 1:
                    self.setEdge(node, Direction.DOWN, Direction.UP)
                elif dirc == 2:
                    self.setEdge(node, Direction.LEFT, Direction.RIGHT)
                else:
                    self.setEdge(node, Direction.RIGHT, Direction.LEFT)

    def findNodeOutsideTree(self):
        node = self.pickRandomNode()
        hasNeighboursOnTree = self.verifyNeighbours(node)
        while node.isOnGraph() == True or hasNeighboursOnTree == False:
            node = self.pickRandomNode()
            hasNeighboursOnTree = self.verifyNeighbours(node)
        return node
                
    def pickRandomNode(self):
        return self.getNode(random.randint(0, self.width - 1), random.randint(0, self.height - 1))

    def setFirstRandomNode(self):
        x = random.randint(0, self.width - 1)
        y = random.randint(0, self.height - 1)
        self.nodeList[x][y].setOnGraph()

    def verifyNeighbours(self, node):
        result = False
        if isinstance(node, Node):
            result = self.verifyNeighbourOnThisDirection(node, result, Direction.UP)
            result = self.verifyNeighbourOnThisDirection(node, result, Direction.DOWN)
            result = self.verifyNeighbourOnThisDirection(node, result, Direction.LEFT)
            result = self.verifyNeighbourOnThisDirection(node, result, Direction.RIGHT)
        return result

    def verifyNeighbourOnThisDirection(self, node, hasNeighboursOnTree, direction):
        if isinstance(node, Node) and isinstance(hasNeighboursOnTree, bool) and isinstance(direction, Direction):
            neighbour = self.getNeighbour(node, direction)
            hasNeighboursOnTree = hasNeighboursOnTree or (neighbour != None and neighbour.isOnGraph())
        return hasNeighboursOnTree

    def getNeighbour(self, node, direction):
        result = None
        if isinstance(node, Node) and isinstance(direction, Direction):
            p = node.getPosition().p
            if direction == Direction.UP:
                if p[1] > 0:
                    result = self.getNode(p[0], p[1] - 1)
            elif direction == Direction.DOWN:
                if p[1] < self.height - 1:
                    result = self.getNode(p[0], p[1] + 1)
            elif direction == Direction.RIGHT:
                if p[0] < self.width - 1:
                    result = self.getNode(p[0] + 1, p[1])
            elif direction == Direction.LEFT:
                if p[0] > 0:
                    result = self.getNode(p[0] - 1, p[1])
        return result

    def setEdge(self, node, directionTo, directionFrom):
        if isinstance(node, Node) and isinstance(directionTo, Direction) and isinstance(directionFrom, Direction):
            neighbour = self.getNeighbour(node, directionTo)
            if neighbour != None and neighbour.isOnGraph():
                node.setEdge(directionTo)
                node.setOnGraph()
                neighbour.setEdge(directionFrom)

    def solve():
        if self.solution == None:
            startPoint = Point(0, 0)
            endPoint = Point(self.getWidth() - 1, self.getHeight() - 1)
            startState = State(None, startPoint, 0, startPoint.distanteTo(endPoint))
            endState = State(None, endPoint, endPoint, startPoint.distanceTo(endPoint), startPoint.distanceTo(endPoint))
            aStar = AStar(startState, endState, self)
            this.solution = aStar.findSolution()

# ---------------------------------------------------------------------------- #
#                                Drawing part                                  #
# ---------------------------------------------------------------------------- #

    def draw(self, walls, floor, square_size, dx, dy):
        for i in range(self.getWidth()):
            for j in range(self.getHeight()):
                self.getNode(i, j).draw(dx + i * square_size, dy + j * square_size, walls, floor, square_size)
