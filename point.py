# cmps5P
# Project Maze Escape
# 1506372

import math
from numbers import Number

class Point(object):
    def __init__(self, *args):
        # contructor for a pair of integer coordinates
        if len(args) == 2 and isinstance(args[0], int) and isinstance(args[1], int):
            self.p = (args[0], args[1])
        # contructor for a pair of non integer coordinates
        if len(args) == 2 and isinstance(args[0], Number) and isinstance(args[1], Number):
            self.p = (int(args[0]), int(args[1]))
        # constructor that takes another point as input and copies it
        elif len(args) == 1 and isinstance(args[0], Point):
            self.p = (args[0].p[0], args[0].p[1])
        # default null constructor
        else:
            self.p = (0, 0)
            
    def __str__(self):
        return str(self.p)

    def getLocation(self):
        return (self.p[0], self.p[1])

    def setLocation(self, *args):
        if len(args) == 2 and isinstance(args[0], int) and isinstance(args[1], int):
            self.p = (args[0], args[1])
        elif len(args) == 2 and isinstance(args[0], Number) and isinstance(args[1], Number):
            self.p = (int(args[0]), int(args[1]))
        elif len(args) == 1 and isinstance(args[0], Point):
            self.setLocation(args[0].p[0], args[0].p[1])

    def move(self, x, y):
        if isinstance(x, int) and isinstance(y, int):
            self.p = (x, y)
        elif isinstance(x, Number) and isistance(y, Number):
            self.p = (int(x), int(y))

    def translate(self, dx, dy):
        if isinstance(dx, int) and isinstance(dy, int):
            self.p = (self.p[0] + dx, self.p[1] + dy)
        if isinstance(dx, int) and isinstance(dy, int):
            self.p = (int(self.p[0] + dx), int(self.p[1] + dy))

    def distanceTo(self, p):
        return int(math.fabs(self.p[0] - p.p[0]) + math.fabs(self.p[1] - p.p[1]))

    def __eq__(self, other):
        if isinstance(other, Point):
            return self.distanceTo(other) == 0
    def __ne__(self, other):
        return not self.__eq__(other)
