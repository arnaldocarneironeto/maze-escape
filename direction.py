# cmps5P
# Project Maze Escape
# 1506372

from enum import Enum

class Direction(Enum):
    UP, RIGHT, DOWN, LEFT = range(4)

    def __init__(self, index):
        self.index = index

    def getIndex(self):
        return self.index
