# cmps5P
# Project Maze Escape
# Bruno de Carvalho Volpe
# Arnaldo Carneiro da Silva Neto
# 1506456
# 1506372

import pyglet
from tree import Tree
from pyglet.window import key
from pyglet.gl import *
from stage import Stage
from character import Character
from direction import Direction
from time import *
from astar import AStar
from point import Point

window = pyglet.window.Window(fullscreen = True)
dims = window.get_size()
ratio = dims[0] / dims[1]
pathDrawed = False
level = 7

def reset():
    global x
    global y
    global seq
    global t
    global param_time
    global start_time
    global finished
    global c
    global p
    global st
    global path
    global height
    global width
    global square_size
    global dx
    global dy
    height = level
    width = int(height * ratio)
    square_size = int(min(dims[0] / width, dims[1] / height))
    dx = (dims[0] - width * square_size) // 2
    dy = (dims[1] - height * square_size) // 2
    x = 0
    y = 0
    seq = 0
    t = Tree(width, height)
    t.generate()
    aStar = AStar(Point(0, 0), Point(width - 1, height -1), t)
    path = aStar.findSolution()
    param_time = len(path) * 0.25
    start_time = time()
    finished = False
    st = Stage(t, square_size, "walls.png", "floor.png")
    c = Character(square_size, "skeleton_4.png", 0)
    p = Character(square_size, "teleporter.png", 0)

reset()

@window.event
def on_key_press(symbol, modifiers):
    global x
    global y
    global testKeyboardPieces
    global success
    global finished
    global pathDrawed
    global level
    node = t.getNode(x, y)
    if symbol == key.LEFT and node.hasEdge(Direction.LEFT) and not finished:
        x = x - 1
        c.set_direction(2)
    elif symbol == key.RIGHT and node.hasEdge(Direction.RIGHT) and not finished:
        x = x + 1
        c.set_direction(0)
    elif symbol == key.DOWN and node.hasEdge(Direction.UP) and not finished:
        y = y - 1
        c.set_direction(1)
    elif symbol == key.UP and node.hasEdge(Direction.DOWN) and not finished:
        y = y + 1
        c.set_direction(3)
    elif symbol == key.ENTER:
        reset()
    elif symbol == key.F1:
        pathDrawed = not pathDrawed
    elif symbol == key.F3:
        level += 1
        reset()
    elif symbol == key.F2:
        level -= 1
        reset()
    if x == width - 1 and y == height - 1 and not finished:
        success = time() - start_time < param_time
        finished = True

@window.event
def on_draw():
    window.clear()
    st.draw(dx, dy)
    p.draw(width - 1, height- 1, dx, dy, seq)
    if pathDrawed:
        for tile in path:
            draw_rect(tile[0] * square_size + square_size/2 + dx,
                      tile[1] * square_size + int(2 * square_size / 5) + dy,
                      square_size / 5,
                      square_size / 5,
                      (255, 0, 0, 0))
    if finished:
        if success:
            draw_text("You win.")
        else:
            draw_text("Try again.")
    else:
        etime = time() - start_time
        ltime = int(10 * (param_time - etime)) / 10
        label = pyglet.text.Label(str(ltime),
                              font_name='Times New Roman',
                              font_size=36,
                              x=10, y=window.height,
                              anchor_x='left', anchor_y='top')
        if ltime >= 0:
            label.draw()
    c.draw(x, y, dx, dy, seq)

def draw_text(text):
    label = pyglet.text.Label(text,
                          font_name='Times New Roman',
                          font_size=72,
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')
    label.draw()

def draw_rect(x, y, width, height, color):
    width = int(round(width))
    height = int(round(height))
    image_pattern = pyglet.image.SolidColorImagePattern(color=color)
    image = image_pattern.create_image(width, height)
    image.blit(x, y)

def update(dt):
    global seq
    global success
    global finished
    seq = int(seq + 1) % 4
    if time() - start_time > param_time and not finished:
        success = False
        finished = True
    
pyglet.clock.schedule_interval(update, 0.1)
pyglet.app.run()
quit()
