# cmps5P
# Project Maze Escape
# 1506372

import pyglet
from pyglet.gl import *

DIM = (4, 9)

class Character(object):
    def __init__(self, tile_size, character_file, direction):
        self.tile_size = tile_size
        self.sprites = load_sprites(character_file, tile_size)
        self.direction = direction

    def set_direction(self, direction):
        self.direction = direction

    def rotate(self):
        self.direction = (self.direction + 1) % DIM[0]

    def draw(self, x, y, dx, dy, seq):
        self.sprites[seq][self.direction].x = x * self.tile_size + (self.tile_size + dx) // 2
        self.sprites[seq][self.direction].y = y * self.tile_size + (2 * self.tile_size // 5) - dy
        self.sprites[seq][self.direction].draw()

def load_sprites(fn, tile_size):
    try:
        part = pyglet.image.load(fn)
        k1 = int(1/DIM[1]*part.width)
        k2 = int(1/DIM[0]*part.height)
        images = [[part.get_region(x=k1*i, y=k2*j, width=k1, height=k2).get_texture() for j in range(DIM[0])] for i in range(DIM[1])]
        tiles = []
        for i in range(DIM[1]):
            tiles.append([])
            for j in range(DIM[0]):
                gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
                images[i][j].width = tile_size // 2
                images[i][j].height = tile_size // 2
                tiles[i].append(pyglet.sprite.Sprite(images[i][j]))
        return tiles
    except IOError:
        print("IO Error - are you sure the image is available?. {0}".format(fn))
