# cmps5P
# Project Maze Escape
# 1506372

from point import Point
from direction import Direction
from numbers import Number
import pyglet

class Node(object):
    def __init__(self, x, y):
        self.onGraph = False
        self.edges = [False for dir in Direction]
        self.position = Point(x, y)

    def __str__(self):
        return "On graph = " + str(self.onGraph) + "\nEdges = " + str(self.edges) + "\nPosition = " + str(self.position)

    def isOnGraph(self):
        return self.onGraph

    def setOnGraph(self):
        self.onGraph = True

    def hasEdge(self, dir):
        if isinstance(dir, Direction):
            return self.edges[dir.getIndex()]
        else:
            return False

    def setEdge(self, dir):
        if isinstance(dir, Direction):
            self.edges[dir.getIndex()] = True

    def getPosition(self):
        return self.position

    def setPosition(self, *args):
        if len(args) == 1 and isinstance(args[0], Point):
            self.position = args[0]
        elif len(args) == 2 and isinstance(args[0], Number) and isinstance(args[1], Number):
            self.position = Point(args[0], args[1])

# ---------------------------------------------------------------------------- #
#                                Drawing part                                  #
# ---------------------------------------------------------------------------- #

    def draw(self, x, y, walls, floor, square_size):
        self.draw_up_left_corner(x, y, walls, floor, square_size // 2)
        self.draw_up_right_corner(x, y, walls, floor, square_size // 2)
        self.draw_down_left_corner(x, y, walls, floor, square_size // 2)
        self.draw_down_right_corner(x, y, walls, floor, square_size // 2)

    def draw_up_left_corner(self, x, y, walls, floor, square_size):
        if self.hasEdge(Direction.UP) and self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y, walls[1][1], floor[1][1])
        if self.hasEdge(Direction.UP) and not self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y, walls[0][1], floor[0][1])
        if not self.hasEdge(Direction.UP) and self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y, walls[1][0], floor[1][0])
        if not self.hasEdge(Direction.UP) and not self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y, walls[0][0], floor[0][0])
    
    def draw_up_right_corner(self, x, y, walls, floor, square_size):
        if self.hasEdge(Direction.UP) and self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y, walls[1][1], floor[1][1])
        if self.hasEdge(Direction.UP) and not self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y, walls[2][1], floor[2][1])
        if not self.hasEdge(Direction.UP) and self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y, walls[1][0], floor[1][0])
        if not self.hasEdge(Direction.UP) and not self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y, walls[2][0], floor[2][0])
    
    def draw_down_left_corner(self, x, y, walls, floor, square_size):
        if self.hasEdge(Direction.DOWN) and self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y + square_size, walls[1][1], floor[1][1])
        if self.hasEdge(Direction.DOWN) and not self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y + square_size, walls[0][1], floor[0][1])
        if not self.hasEdge(Direction.DOWN) and self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y + square_size, walls[1][2], floor[1][2])
        if not self.hasEdge(Direction.DOWN) and not self.hasEdge(Direction.LEFT):
            self.draw_piece(x, y + square_size, walls[0][2], floor[0][2])
    
    def draw_down_right_corner(self, x, y, walls, floor, square_size):
        if self.hasEdge(Direction.DOWN) and self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y + square_size, walls[1][1], floor[1][1])
        if self.hasEdge(Direction.DOWN) and not self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y + square_size, walls[2][1], floor[2][1])
        if not self.hasEdge(Direction.DOWN) and self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y + square_size, walls[1][2], floor[1][2])
        if not self.hasEdge(Direction.DOWN) and not self.hasEdge(Direction.RIGHT):
            self.draw_piece(x + square_size, y + square_size, walls[2][2], floor[2][2])

    def draw_piece(self, x, y, wall_sprite, floor_sprite):
        floor_sprite.x = x
        floor_sprite.y = y
        floor_sprite.draw()
        wall_sprite.x = x
        wall_sprite.y = y
        wall_sprite.draw()
