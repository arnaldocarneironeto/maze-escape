# cmps5P
# Project Maze Escape
# 1506372

import pyglet
from tree import Tree
from pyglet.window import key
from pyglet.gl import *

class Stage(object):
    def __init__(self, maze, tile_size, walls_file, floor_file):
        self.maze = maze
        self.tile_size = tile_size
        self.walls = load_tiles(walls_file, tile_size)
        self.floor = load_tiles(floor_file, tile_size)

    def draw(self, dx, dy):
        self.maze.draw(self.walls, self.floor, self.tile_size, dx, dy)

def load_tiles(fn, tile_size):
    try:
        part = pyglet.image.load(fn)
        k1 = int(31/110*part.width)
        k2 = int(47/110*part.width)
        tile_images = [[part.get_region(x=k1*i, y=k1*j, width=k2, height=k2).get_texture() for j in range(3)] for i in range(3)]
        tiles = []
        for i in range(3):
            tiles.append([])
            for j in range(3):
                gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
                tile_images[i][j].width = tile_size // 2
                tile_images[i][j].height = tile_size // 2
                tiles[i].append(pyglet.sprite.Sprite(tile_images[i][j]))
        return tiles
    except IOError:
        print("IO Error - are you sure the image is available?. {0}".format(fn))
